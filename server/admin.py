"""
Project name: houndfox
Module name: admin.py
Description: File where the models are registered, in order to be
             able to access them from the administration interface.

Authors: Pedro Jose Cazorla Garcia and Beatriz Romera de Blas
Date: 27-November-2016

Computer Systems Project, assignment 3
"""

from django.contrib import admin
from server.models import Game, Move, Counter

# We register all the models of the database.
admin.site.register(Game)
admin.site.register(Move)
admin.site.register(Counter)