"""
Project name: houndfox
Module name: tests.py
Description: Definition of the Python tests used to check that the
             application works properly. Some minor modifications
             have been made, after having talked to the teacher.

Authors: Pedro Jose Cazorla Garcia and Beatriz Romera de Blas
Date: 27-November-2016

Computer Systems Project, assignment 3
"""
from unittest import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.test import Client
from server.models import Game, Move, Counter

usernameCat = 'gatoUser'
passwdCat = 'gatoPasswd'
usernameMouse = 'ratonUser'
passwdMouse = 'ratonPasswd'

usernameCat2 = 'gatoUser2'
passwdCat2 = 'gatoPasswd2'
usernameMouse2 = 'ratonUser2'
passwdMouse2 = 'ratonPasswd2'

DEBUG = False

class SimultaneousTest(TestCase):

    def setUp(self):
        self.clientCat   = Client()
        self.clientMouse = Client()
        self.clientCat2 = Client()
        self.clientMouse2 = Client()

    def creatUser(self, userName, userPassword):
        try:
            user = User.objects.get(username=userName)
        except User.DoesNotExist:
            user = User(username=userName, password=userPassword)
            user.set_password(user.password)
            user.save()
        return user.id

    def login(self, userName, userPassword, client):
        response = client.get(reverse('login_user'))
        self.assertIn(b'Login', response.content)
        loginDict={}
        loginDict["username"]=userName
        loginDict["password"]=userPassword
        response = client.post(reverse('login_user'), loginDict, follow=True)
        return response


    ###############################################################
    # Function: test_simultaneous
    # Description: Function testing that the same users can play two different
    #              games at the same time, without any of them affecting the other.
    # Parameters: None, apart from the test class.
    # Author: Pedro Jose Cazorla Garcia
    ###############################################################
    def test_simultaneous(self):

        # We make sure the cat users exist
        userCatId = self.creatUser(usernameCat, passwdCat)
        userCat2Id = self.creatUser(usernameCat2, passwdCat2)

        # Same with the mouse users
        userMouseID = self.creatUser(usernameMouse, passwdMouse)
        userMouse2ID = self.creatUser(usernameMouse2, passwdMouse2)

        # delete all orphan pages, to make sure we get into the game we
        # will create now
        response = self.clientCat.get(reverse('clean_orphan_games'))

        # We login in all four 'browsers'
        response = self.login(usernameCat, passwdCat, self.clientCat)
        response = self.login(usernameMouse, passwdMouse, self.clientMouse)
        response = self.login(usernameCat2, passwdCat2, self.clientCat2)
        response = self.login(usernameMouse2, passwdMouse2, self.clientMouse2)

        # We create and join the two games
        response = self.clientCat.get(reverse('create_game'))
        self.assertEqual(response.status_code, 200)
        response = self.clientMouse.get(reverse('join_game'))

        response = self.clientCat2.get(reverse('create_game'))
        self.assertEqual(response.status_code, 200)
        response = self.clientMouse2.get(reverse('join_game'))

        # We will check if the two games can be simultaneously played by checking some
        # movements.

        # First movement
        moveDict = {}
        moveDict['origin'] = 2
        moveDict['target'] = 11

        response = self.clientCat.post(reverse('move'), moveDict)
        self.assertEqual(response.status_code, 200)

        self.assertIn(b'houndUser = %s (%d)' % (usernameCat, userCatId), response.content)  # catUser   = gatoUser (1)
        self.assertIn(b'foxUser = %s (%d)' % (usernameMouse, userMouseID), response.content)  # catUser   = gatoUser (1)

        # very likely the second to last game is the game we are interested in.
        secondTolastGame = Game.objects.all().order_by("-id")[1]
        self.assertIn(b'gameId = %d' % secondTolastGame.id, response.content)

        # very likely the last move is the one we just created
        lastMove = Move.objects.all().order_by("-id")[0]

        self.assertIn(b'moveId (origin/target) = %d (%d/%d)' % (lastMove.id, lastMove.origin, lastMove.target),
                      response.content)

        # First movement of the second game.

        # This movement should not be valid, since they are not in the initial position.
        moveDict['origin'] = 11
        moveDict['target'] = 20

        response = self.clientCat2.post(reverse('move'), moveDict)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Cannot create a move', response.content)

        # We try a valid movement this time
        moveDict['origin'] = 0
        moveDict['target'] = 9

        response = self.clientCat2.post(reverse('move'), moveDict)
        self.assertEqual(response.status_code, 200)

        self.assertIn(b'houndUser = %s (%d)' % (usernameCat2, userCat2Id), response.content)  # catUser   = gatoUser (1)
        self.assertIn(b'foxUser = %s (%d)' % (usernameMouse2, userMouse2ID), response.content)  # catUser   = gatoUser (1)

        # very likely the second to last game is the game we are interested in.
        lastGame = Game.objects.all().order_by("-id")[0]
        self.assertIn(b'gameId = %d' % lastGame.id, response.content)

        # very likely the last move is the one we just created
        lastMove = Move.objects.all().order_by("-id")[0]

        self.assertIn(b'moveId (origin/target) = %d (%d/%d)' % (lastMove.id, lastMove.origin, lastMove.target),
                      response.content)

        # Second movement of the first game:
        moveDict['origin'] = 59
        moveDict['target'] = 52

        response = self.clientMouse.post(reverse('move'), moveDict)
        self.assertEqual(response.status_code, 200)

        self.assertIn(b'gameId = %d' % secondTolastGame.id, response.content)

        lastMove = Move.objects.all().order_by("-id")[0]

        self.assertIn(b'moveId (origin/target) = %d (%d/%d)' % (lastMove.id, lastMove.origin, lastMove.target),
                      response.content)

        # Third movement of the first game:
        moveDict['origin'] = 0
        moveDict['target'] = 9

        response = self.clientCat.post(reverse('move'), moveDict)
        self.assertEqual(response.status_code, 200)

        self.assertIn(b'gameId = %d' % secondTolastGame.id, response.content)

        # very likely the last move is the one we just created
        lastMove = Move.objects.all().order_by("-id")[0]

        self.assertIn(b'moveId (origin/target) = %d (%d/%d)' % (lastMove.id, lastMove.origin, lastMove.target),
                      response.content)

        # Second movement of the last game.

        moveDict['origin'] = 59
        moveDict['target'] = 52

        response = self.clientMouse2.post(reverse('move'), moveDict)
        self.assertEqual(response.status_code, 200)

        self.assertIn(b'gameId = %d' % lastGame.id, response.content)

        # very likely the last move is the one we just created
        lastMove = Move.objects.all().order_by("-id")[0]

        self.assertIn(b'moveId (origin/target) = %d (%d/%d)' % (lastMove.id, lastMove.origin, lastMove.target),
                      response.content)
