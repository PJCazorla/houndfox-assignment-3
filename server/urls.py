"""
Project name: houndfox
Module name: urls.py
Description: Map of URLs of the application server to the different
             functions in views, with a name we shall use to
             identify them.

Authors: Pedro Jose Cazorla Garcia and Beatriz Romera de Blas
Date: 27-November-2016

Computer Systems Project, assignment 3
"""

from django.conf.urls import url
from django.contrib import admin
from django.conf.urls import include
from server import views

# List of URL patterns.
urlpatterns = [

    # Both 'server' and 'server/index' go to the index page.
    url(r'^$', views.index, name='index'),
    url(r'^index/', views.index, name='index'),

    # Counter page.
    url(r'^counter/', views.counterSession, name='counter'),

    # Registration page.
    url(r'^register_user/', views.register_user, name='register_user'),

    # Login page.
    url(r'^login_user/', views.login_user, name='login_user'),

    # Logout page.
    url(r'^logout_user/', views.logout_user, name='logout_user'),

    # Game creation page.
    url(r'^create_game/', views.create_game, name='create_game'),

    # Error page to show when login is required, but the user hasn't
    # logged into the system. The route is defined in settings.py
    url(r'^nologged/', views.nologged, name='nologged'),

    # Page to clean games without foxUser
    url(r'^clean_orphan_games/', views.clean_orphan_games, name='clean_orphan_games'),

    # Page to join game as a fox.
    url(r'^join_game/', views.join_game, name='join_game'),

    # Page to make a move.
    url(r'^move/', views.move, name='move'),

    # Page to check if it is our turn.
    url(r'^status_turn/', views.status_turn, name='status_turn'),

    # Page to represent the board graphically.
    url(r'^status_board/', views.status_board, name='status_board'),
]